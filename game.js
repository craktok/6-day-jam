var background = new Image(800, 800);
background.src = './img/background.png';

var resourceImage = new Image(40, 40);
resourceImage.src = './img/coin.png';

var victoryImage = new Image(40, 40);
victoryImage.src = './img/flag.png';

var c = document.getElementById("gameCanvas");
var g = c.getContext("2d");

var width = 800;
var height = 800;

var currentPlayer = 0;

var units = [];
units.push(new Fast(0, 1, 1));
units.push(new Spear(0, 1, 2));
units.push(new Fast(1, 18, 18));
units.push(new Spear(1, 18, 17));
var selected = units[0];
selected.selected = true;

var MOVE = 0;
var ATTACK = 1;
var BUILD = 2;

var SWORD_COST = 100;
var ARCHER_COST = 150;
var TANK_COST = 200;
var SPEAR_COST = 75;
var FAST_COST = 50;

var currentPhase = 0;

var gold = [0, 0];
var victoryPoints = [0, 0];

var resource = [];
resource.push([4, 4]);
resource.push([14, 5]);
resource.push([5, 14]);
resource.push([15, 15]);

var victory = [];
victory.push([8, 8]);
victory.push([11, 11]);

playerText = document.getElementById("player");
phaseText = document.getElementById("phase");
goldText = document.getElementById("gold");
vpText = document.getElementById("vp");

moveText = document.getElementById("move");
healthText = document.getElementById("health");


function draw() {
	g.clearRect(0, 0, 800, 800);
	g.drawImage(background, 0, 0);
	for (var i = 0; i < resource.length; i++) {
		g.drawImage(resourceImage, resource[i][0] * 40, resource[i][1] * 40);
	}
	for (var i = 0; i < victory.length; i++) {
		g.drawImage(victoryImage, victory[i][0] * 40, victory[i][1] * 40);
	}
	for (i = 0; i < units.length; i++) {
		units[i].draw(g);
	}
	if (selected) {
		if (currentPhase === MOVE) {
			g.fillStyle = "rgba(0, 255, 0, 0.25)";
			for (i = 1; i <= selected.currentMove; i++) {
				for (j = 0; j < i; j++) {
					g.fillRect((selected.x + i - j) * 40, (selected.y - j) * 40, 40, 40);
					g.fillRect((selected.x - i + j) * 40, (selected.y + j) * 40, 40, 40);
					g.fillRect((selected.x - j) * 40, (selected.y - i + j) * 40, 40, 40);
					g.fillRect((selected.x + j) * 40, (selected.y + i - j) * 40, 40, 40);
				}
			}
		}
		if (currentPhase === ATTACK && selected.canAttack) {
			g.fillStyle = "rgba(225, 0, 0, 0.25)";
			for (i = 1; i <= selected.range; i++) {
				for (j = 0; j < i; j++) {
					g.fillRect((selected.x + i - j) * 40, (selected.y - j) * 40, 40, 40);
					g.fillRect((selected.x - i + j) * 40, (selected.y + j) * 40, 40, 40);
					g.fillRect((selected.x - j) * 40, (selected.y - i + j) * 40, 40, 40);
					g.fillRect((selected.x + j) * 40, (selected.y + i - j) * 40, 40, 40);
				}
			}
		}
	}
	g.fillStyle = "rgb(255, 255, 255)";
}

function gameLoop() {
	draw();
	setTimeout(gameLoop, 33);
}

c.onclick = function(event) {
	var x = Math.floor(event.offsetX / 40);
	var y = Math.floor(event.offsetY / 40);
	if (currentPhase === ATTACK && selected.canAttack) {
		var distance = Math.abs(selected.x - x) + Math.abs(selected.y - y);
		if (distance <= selected.range) {
			for (i = 0; i < units.length; i++) {
				if (units[i].x === x && units[i].y == y) {
					if (selected.team !== units[i].team) {
						selected.canAttack = false;
						units[i].health -= selected.damage;
						if (units[i].health <= 0) {
							units.splice(i, 1);
						}
						nextUnit();
						break;
					}
				}
			}
		}
	}
	if (currentPhase === MOVE) {
		distance = Math.abs(selected.x - x) + Math.abs(selected.y - y)
		if (distance <= selected.currentMove) {
			for (i = 0; i < units.length; i++) {
				if (units[i].x === x && units[i].y == y) {
					return;
				}
			}
			selected.x = x;
			selected.y = y;
			selected.currentMove -= distance;
			if (selected.currentMove < 1) {
				nextUnit();
			}
		}
	}
};

window.onkeydown = function(event) {
	// Tab key
	if (event.keyCode === 9) {
		nextUnit();
	}
	event.preventDefault();
};

function checkCollision(x, y) {
	for (i = 0; i < units.length; i++) {
		if (units[i].x == x && units[i].y == y) {
			return true;
		}
	}
	return false;
}

function passTurn() {
	hideBuildMenu();
	currentPhase = 0;
	if (selected) {
		selected.selected = false;
	}
	selected = null;
	currentPlayer = (currentPlayer + 1) % 2;
	if (currentPlayer === 0) {
		for (i = 0; i < resource.length; i++) {
			for (j = 0; j < units.length; j++) {
				if (resource[i][0] === units[j].x && resource[i][1] === units[j].y) {
					gold[units[j].team] += 100;
				}
			}
		}
		for (i = 0; i < victory.length; i++) {
			for (j = 0; j < units.length; j++) {
				if (victory[i][0] === units[j].x && victory[i][1] === units[j].y) {
					victoryPoints[units[j].team] += 1;
				}
			}
		}
	}
	for (i = 0; i < units.length; i++) {
		units[i].newTurn();
	}
	for (i = 0; i < units.length; i++) {
		if (units[i].team === currentPlayer) {
			selected = units[i];
			selected.selected = true;
			break;
		}
	}
	phaseText.innerHTML = "Phase: Move";
	playerText.innerHTML = "Player " + (currentPlayer + 1);
	vpText.innerHTML = "Victory Points: " + victoryPoints[currentPlayer];
	updateGold();
}

function nextPhase() {
	if (currentPhase < BUILD) {
		currentPhase++;
		if (currentPhase === BUILD) {
			showBuildMenu();
		}
	}
	if (currentPhase === MOVE) {
		phaseText.innerHTML = "Phase: Move";
	}
	else if (currentPhase === ATTACK) {
		phaseText.innerHTML = "Phase: Attack";
	}
	else {
		phaseText.innerHTML = "Phase: Build";
	}
	//selected.selected = false;
	//selected = null;
	//nextUnit();
}

function nextUnit() {
	var found = false;
	var i = 0;
	if (selected) {
		i = units.indexOf(selected) + 1;
	}
	for (; i < units.length && !found; i++) {
		if (units[i].team === currentPlayer) {
			selected.selected = false;
			selected = units[i];
			selected.selected = true;
			found = true;
		}
	}
	for (i = 0; i < units.length && !found; i++) {
		if (units[i].team === currentPlayer) {
			selected.selected = false;
			selected = units[i];
			selected.selected = true;
			found = true;
		}
	}
	moveText.innerHTML = "Move: " + selected.currentMove;
	healthText.innerHTML = "Health: " + selected.health;
}

var buildMenu = document.getElementById("build");

function showBuildMenu() {
	build.style.display = "block";
}

function hideBuildMenu() {
	build.style.display = "none";
}

function findSpawn() {
	var x = 0;
	var y = 0;
	if (currentPlayer === 0) {
		x = 0;
		y = 0;
	}
	if (currentPlayer === 1) {
		x = 19;
		y = 19;
	}
	return [x, y];
}

function updateGold() {
	goldText.innerHTML = "Gold: " + gold[currentPlayer];
}

function buildSword() {
	if (gold[currentPlayer] < SWORD_COST) {
		return;
	}
	gold[currentPlayer] -= SWORD_COST;
	var spawn = findSpawn();
	units.push(new Sword(currentPlayer, spawn[0], spawn[1]));
	updateGold();
}

function buildArcher() {
	if (gold[currentPlayer] < ARCHER_COST) {
		return;
	}
	gold[currentPlayer] -= ARCHER_COST;
	var spawn = findSpawn();
	units.push(new Archer(currentPlayer, spawn[0], spawn[1]));
	updateGold();
}

function buildTank() {
	if (gold[currentPlayer] < TANK_COST) {
		return;
	}
	gold[currentPlayer] -= TANK_COST;
	var spawn = findSpawn();
	units.push(new Tank(currentPlayer, spawn[0], spawn[1]));
	updateGold();
}

function buildSpear() {
	if (gold[currentPlayer] < SPEAR_COST) {
		return;
	}
	gold[currentPlayer] -= SPEAR_COST;
	var spawn = findSpawn();
	units.push(new Spear(currentPlayer, spawn[0], spawn[1]));
	updateGold();
}

function buildFast() {
	if (gold[currentPlayer] < FAST_COST) {
		return;
	}
	gold[currentPlayer] -= FAST_COST;
	var spawn = findSpawn();
	units.push(new Fast(currentPlayer, spawn[0], spawn[1]));
	updateGold();
}

nextUnit();
gameLoop();