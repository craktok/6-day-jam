var fastImage = [];
fastImage.push(new Image(40, 40));
fastImage.push(new Image(40, 40));
fastImage[0].src = './img/fast1.png';
fastImage[1].src = './img/fast2.png';

function Fast(team, x, y) {
	this.health = 50;
	this.maxHealth = 50;
	this.damage = 5;
	this.move = 10;
	this.currentMove = 10;
	this.team = team;
	this.hasMoved = false;
	this.x = x;
	this.y = y;
	this.selected = false;
	this.range = 1;
	this.canAttack = true;
	
	this.draw = function(g) {
		g.strokeStyle = "#13A610";
		if (this.selected) {
			g.strokeRect(this.x * 40, this.y * 40, 40, 40);
		}
		g.drawImage(fastImage[this.team], this.x * 40, this.y * 40);
		g.strokeStyle = this.team == 0 ? "#0000FF" : "#FF0000";
		g.strokeRect(this.x * 40, this.y * 40, 40, 5);
		g.fillStyle = this.team == 0 ? "#0000FF" : "#FF0000";
		g.fillRect(this.x * 40, this.y * 40, 40 * (this.health / this.maxHealth), 5)
	};
	
	this.newTurn = function() {
		this.currentMove = this.move;
		this.canAttack = true;
	}
}