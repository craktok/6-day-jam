var tankImage = [];
tankImage.push(new Image(40, 40));
tankImage.push(new Image(40, 40));
tankImage[0].src = './img/tank1.png';
tankImage[1].src = './img/tank2.png';

function Tank(team, x, y) {
	this.health = 200;
	this.maxHealth = 200;
	this.damage = 5;
	this.move = 3;
	this.currentMove = 5;
	this.team = team;
	this.hasMoved = false;
	this.x = x;
	this.y = y;
	this.selected = false;
	this.range = 1;
	this.canAttack = true;
	
	this.draw = function(g) {
		g.strokeStyle = "#13A610";
		if (this.selected) {
			g.strokeRect(this.x * 40, this.y * 40, 40, 40);
		}
		g.drawImage(tankImage[this.team], this.x * 40, this.y * 40);
		g.strokeStyle = this.team == 0 ? "#0000FF" : "#FF0000";
		g.strokeRect(this.x * 40, this.y * 40, 40, 5);
		g.fillStyle = this.team == 0 ? "#0000FF" : "#FF0000";
		g.fillRect(this.x * 40, this.y * 40, 40 * (this.health / this.maxHealth), 5)
	};
	
	this.newTurn = function() {
		this.currentMove = this.move;
		this.canAttack = true;
	}
}