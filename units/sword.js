var swordImage = [];
swordImage.push(new Image(40, 40));
swordImage.push(new Image(40, 40));
swordImage[0].src = './img/sword1.png';
swordImage[1].src = './img/sword2.png';

function Sword(team, x, y) {
	this.health = 100;
	this.maxHealth = 100;
	this.damage = 10;
	this.move = 5;
	this.currentMove = 5;
	this.team = team;
	this.hasMoved = false;
	this.x = x;
	this.y = y;
	this.selected = false;
	this.range = 1;
	this.canAttack = true;
	
	this.draw = function(g) {
		g.strokeStyle = "#13A610";
		if (this.selected) {
			g.strokeRect(this.x * 40, this.y * 40, 40, 40);
		}
		g.drawImage(swordImage[this.team], this.x * 40, this.y * 40);
		g.strokeStyle = this.team == 0 ? "#0000FF" : "#FF0000";
		g.strokeRect(this.x * 40, this.y * 40, 40, 5);
		g.fillStyle = this.team == 0 ? "#0000FF" : "#FF0000";
		g.fillRect(this.x * 40, this.y * 40, 40 * (this.health / this.maxHealth), 5)
	};
	
	this.newTurn = function() {
		this.currentMove = this.move;
		this.canAttack = true;
	}
}