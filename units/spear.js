var spearImage = [];
spearImage.push(new Image(40, 40));
spearImage.push(new Image(40, 40));
spearImage[0].src = './img/spear1.png';
spearImage[1].src = './img/spear2.png';

function Spear(team, x, y) {
	this.health = 50;
	this.maxHealth = 50;
	this.damage = 10;
	this.move = 6;
	this.currentMove = 5;
	this.team = team;
	this.hasMoved = false;
	this.x = x;
	this.y = y;
	this.selected = false;
	this.range = 2;
	this.canAttack = true;
	
	this.draw = function(g) {
		g.strokeStyle = "#13A610";
		if (this.selected) {
			g.strokeRect(this.x * 40, this.y * 40, 40, 40);
		}
		g.drawImage(spearImage[this.team], this.x * 40, this.y * 40);
		g.strokeStyle = this.team == 0 ? "#0000FF" : "#FF0000";
		g.strokeRect(this.x * 40, this.y * 40, 40, 5);
		g.fillStyle = this.team == 0 ? "#0000FF" : "#FF0000";
		g.fillRect(this.x * 40, this.y * 40, 40 * (this.health / this.maxHealth), 5)
	};
	
	this.newTurn = function() {
		this.currentMove = this.move;
		this.canAttack = true;
	}
}