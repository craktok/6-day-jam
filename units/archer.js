var archerImage = [];
archerImage.push(new Image(40, 40));
archerImage.push(new Image(40, 40));
archerImage[0].src = './img/archer1.png';
archerImage[1].src = './img/archer2.png';

function Archer(team, x, y) {
	this.health = 50;
	this.maxHealth = 50;
	this.damage = 5;
	this.move = 5;
	this.currentMove = 5;
	this.team = team;
	this.hasMoved = false;
	this.x = x;
	this.y = y;
	this.selected = false;
	this.range = 5;
	this.canAttack = true;
	
	this.draw = function(g) {
		g.strokeStyle = "#13A610";
		if (this.selected) {
			g.strokeRect(this.x * 40, this.y * 40, 40, 40);
		}
		g.drawImage(archerImage[this.team], this.x * 40, this.y * 40);
		g.strokeStyle = this.team == 0 ? "#0000FF" : "#FF0000";
		g.strokeRect(this.x * 40, this.y * 40, 40, 5);
		g.fillStyle = this.team == 0 ? "#0000FF" : "#FF0000";
		g.fillRect(this.x * 40, this.y * 40, 40 * (this.health / this.maxHealth), 5)
	};
	
	this.newTurn = function() {
		this.currentMove = this.move;
		this.canAttack = true;
	}
}